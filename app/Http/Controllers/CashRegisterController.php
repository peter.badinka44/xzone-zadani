<?php

namespace App\Http\Controllers;

use App\Models\CashRegister;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CashRegisterController extends Controller
{

    public function create(
        Request $request,
        Response $response,
        CashRegister $model
    ): Response
    {
        $data = $request->all();
        $data['user_id'] = 1;

        $model->create($data);

        return response(
            $response::$statusTexts['201'],
            $response::HTTP_CREATED
        );
    }

    public function getLast(
        Response $response,
        CashRegister $model
    ): Response
    {
        $data = $model::where('user_id', '=', 1)
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->first();

        return response($data, $response::HTTP_OK);
    }
}
