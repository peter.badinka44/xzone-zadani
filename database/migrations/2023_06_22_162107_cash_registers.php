<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('cash_registers', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('cislo_pokladny');
            $table->integer('cislo_predajny');
            $table->float('celkova_castka');
            $table->float('rezerva');
            $table->float('stranou_do_banky');
            $table->json('bankovky');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('cash_registers');
    }
};
